class UsersController < ApplicationController
	def index
		@users = User.all
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new params[:user]

		if @user.save(@user)
			redirect_to users_path, :notice => 'The user has been successfully added.'
		else
			redirect_to new_user_path, :alert => 'There was an error adding the user.'
		end
	end

	def edit
		@user = User.find params[:id]
	end

	def show
	    @user = User.find params[:id]
	end

	def update
		@user = User.find params[:id]

		if @user.update_attributes params[:user]
			redirect_to users_path, :notice => 'The user has been successfully updated.'
		else
			redirect_to edit_user_path, :notice => 'There was an error updating this user.'
		end
	end

	def destroy
		User.destroy params[:id]
		redirect_to users_path, :notice => 'User has been deleted.'
	end
end