# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.delete_all

users = [
  {
    :first_name => 'Jason',
    :last_name => 'Kadrmas',
    :email => 'kadrm002@umn.edu'
  },

  {
    :first_name => 'Aaron',
    :last_name => 'Johnson',
    :email => 'ajj@umn.edu'
  },

  {
    :first_name => 'Ryan',
    :last_name => 'Arneson',
    :email => 'arneson@umn.edu'
  },

  {
    :first_name => 'Ryan',
    :last_name => 'Ricard',
    :email => 'rricard@umn.edu'
  },

  {
    :first_name => 'Gyu',
    :last_name => 'Kwon',
    :email => 'gyu@umn.edu'
  },

  {
    :first_name => 'Barb',
    :last_name => 'Smith',
    :email => 'bjs@umn.edu'
  },

  {
    :first_name => 'Dalia',
    :last_name => 'Rashad',
    :email => 'daila@umn.edu'
  }
]

users.each do |hash|
    user = User.new
    hash.each do |attribute, value|
      user.update_attribute(attribute, value)
    end
end