require "spec_helper"

describe "Users" do
	before do
		@user = User.create :first_name => "Ryan", :last_name => "Arneson", :email => "arneson@gmail.com"
	end

	it "display all users" do      
		visit users_path
		page.should have_content "Ryan Arneson"
	end

	it "displays a single user" do
		visit users_path
		click_link "Show"
		page.should have_content "Ryan Arneson"
	end

	it "creates a new user" do
		visit new_user_path
		fill_in "First Name:", :with => "Lisa"
		fill_in "Last Name:", :with => "Arneson"
		fill_in "Email:", :with => "lisajoy@gmail.com"
		click_button "Create User"
		current_path.should == users_path
		page.should have_content "The user has been successfully added."
	end

	it "edits a user" do
		visit users_path
		click_link "Edit"
		current_path.should == edit_user_path(@user)
		find_field("First Name:").value.should == "Ryan"
		find_field("Last Name:").value.should == "Arneson"
		find_field("Email:").value.should == "arneson@gmail.com"
		fill_in "First Name:", :with => "Samuel"
		fill_in "Last Name:", :with => "Arneson"
		fill_in "Email:", :with => "samuelarneson@gmail.com"
		click_button "Update User"
		page.should have_content "The user has been successfully updated."
	end

	it "should not allow for empty first, last, email" do
		visit users_path
		click_link "Edit"
		fill_in "First Name:", :with => nil
		fill_in "Last Name:", :with => nil
		fill_in "Email:", :with => nil
		click_button "Update User"
		current_path.should == edit_user_path(@user)
		page.should have_content "There was an error updating this user."
	end

	it "deletes a user fromt the list page" do
		visit users_path
		find("#user_#{@user.id}").click_link "Delete"
		page.should have_content "User has been deleted."
		page.should have_no_content "Ryan Arneson"
	end

	it "deletes a user from the show page" do
		visit users_path
		click_link "Show"
		click_link "Delete"
		page.should have_content "User has been deleted."
		page.should have_no_content "Ryan Arneson"
	end
end